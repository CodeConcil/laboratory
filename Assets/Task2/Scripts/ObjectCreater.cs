﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectCreater : MonoBehaviour
{
    public GameObject cube;
    public Text counter;
    public int time = 3;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Timer(time));

    }

    IEnumerator Timer(float time)
    {
     
        while (time >=0)
        {
            
            int temp = (int)time;
            counter.text = temp.ToString();
            yield return new WaitForSeconds(1.0f);
            time--;
        }

        Instantiate<GameObject>(cube);
    }

  
}
