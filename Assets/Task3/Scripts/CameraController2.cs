﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController2 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            this.gameObject.transform.Translate(0.0f, 0.0f, 10.0f * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.S))
        {
            this.gameObject.transform.Translate(0.0f, 0.0f, -10.0f * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.A))
        {
            this.gameObject.transform.Translate(-10.0f * Time.deltaTime, 0.0f, 0);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            this.gameObject.transform.Translate(10.0f * Time.deltaTime, 0.0f, 0);
        }
    }
}
