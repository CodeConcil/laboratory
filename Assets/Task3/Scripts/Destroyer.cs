﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Destroyer : MonoBehaviour
{
    public InputField field;
    private int counter = 0;

    private void Start()
    {
        field.text = counter.ToString();
    }

    private void OnCollisionEnter(Collision collision)
    {
        
        Destroy(collision.gameObject);

        counter++;
        field.text = counter.ToString();
    }
}
