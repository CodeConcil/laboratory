﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
   private AsyncOperation operation;
    // Start is called before the first frame update
    void Start()
    {
        
        StartCoroutine(Loader());
    }

    IEnumerator Loader()
    {
        operation = SceneManager.LoadSceneAsync(1);
        operation.allowSceneActivation = false;
        while (operation.progress < 0.9f)
        {
            yield return null;
        }

    }

    public void LoadScene()
    {
        if (operation.progress >= 0.9f)
            operation.allowSceneActivation = true;
    }

    public void OnTriggerEnter(Collider other)
    {
        if (SceneManager.GetActiveScene().buildIndex == 0)
        LoadScene();
    }
}
